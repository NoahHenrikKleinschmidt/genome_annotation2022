
BUSCO_DIR = "../busco/{source}/{assembly}/"
"""
The destination directory for the generated files.
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The maker output directory.
"""

rule busco:
    input:
        ancient( rules.map_functional.output.protein_fasta ) 
    output:
        directory( BUSCO_DIR ) 
    conda:
        "envs/busco.yaml"
    params:
        busco_lineage = "brassicales_odb10",
        busco_mode = "proteins",
    threads:
        10
    resources:
        tmpdir = "$SCRATCH",
    shell:
        """
        busco -i {input} -o {wildcards.assembly}_busco -l {params.busco_lineage} -m {params.busco_mode} -c {threads};
        mv {wildcards.assembly}_busco {output};
        """

