
DUPGEN_DIR = "../dupgen/{source}/{assembly}/"
"""
The output directory for dupgen
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The maker output directory.
"""

rule make_ref_uniprot_database:
    input:
        uniprot_proteins = "/data/courses/assembly-annotation-course/CDS_annotation/uniprot-plant_reviewed.fasta",
    output:
        "uniprot-plant_reviewed.fasta.db.phr"
    params:
        db = "uniprot-plant_reviewed.fasta.db",
    threads: 
        4
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+; 
        makeblastdb -in {input.uniprot_proteins} -dbtype prot -out {params.db}
        """


rule make_ref_outgroup_database:
    input:
        outgroup_fasta = "/data/courses/assembly-annotation-course/CDS_annotation/NNU.pep.fa.ref.single_model",
    
    output:
        "NNU.pep.fa.ref.single_model.db.phr",
    
    params:
        db = "NNU.pep.fa.ref.single_model.db",
    threads: 
        4
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+;
        makeblastdb -in {input.outgroup_fasta} -dbtype prot -out {params.db};
        """


rule make_database:
    input:
        fasta = MAKER_DIR + "proteins.fasta",

    output:
        MAKER_DIR + "proteins.fasta.db.00.phr",

    params:
        db = MAKER_DIR + "proteins.fasta.db",
    threads: 
        4
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+; 
        makeblastdb -in {input.fasta} -dbtype prot -out {params.db};
        """

rule blast_proteins_against_uniprot:
    input: 
        fasta = MAKER_DIR + "proteins.fasta",
        db = rules.make_ref_uniprot_database.output,
    output: 
        MAKER_DIR + "proteins.blast"
    

    params:
        db = rules.make_ref_uniprot_database.params.db,
        e_value = "1e-10",
        
    threads: 
        15

    resources:
        tmpdir = "$SCRATCH"
    
    # conda:
    #     "envs/blast.yaml"
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+;
        blastp -query {input.fasta} -db {params.db} -outfmt 6 -out {output} -num_threads {threads} -evalue {params.e_value};
        """

rule make_dupgen_database:
    input: 
        fasta = DUPGEN_DIR + "{assembly}.RA.proteins.fasta",
    output: 
        DUPGEN_DIR + "{assembly}.RA.proteins.fasta.db.phr",
    params:
        db = DUPGEN_DIR + "{assembly}.RA.proteins.fasta.db",
    threads:
        10  
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+;
        makeblastdb -in {input.fasta} -dbtype prot -out {params.db};
        """

rule blast_dupgen_ingroup:
    input: 
        fasta = DUPGEN_DIR + "{assembly}.RA.proteins.fasta",
        ingroup_db = rules.make_dupgen_database.output,
    output: 
        ingroup = DUPGEN_DIR + "{assembly}.blast",
    params:
        ingroup_db = rules.make_dupgen_database.params.db
    threads: 
        15
        
    resources:
        tmpdir = "$SCRATCH"
    # conda:
    #     "envs/blast.yaml"
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+;
        blastp -query {input.fasta} -db {params.ingroup_db} -outfmt 6 -out {output.ingroup} -num_threads {threads} -evalue 1e-10 -max_target_seqs 5;
        """

rule blast_dupgen_outgroup:
    input: 
        fasta = DUPGEN_DIR + "{assembly}.RA.proteins.fasta",
        outgroup_db = rules.make_ref_outgroup_database.output,
    output: 
        outgroup = DUPGEN_DIR + "{assembly}_NNU.blast",
    params:
        outgroup_db = rules.make_ref_outgroup_database.params.db,
    threads: 
        15
        
    resources:
        tmpdir = "$SCRATCH"
    # conda:
    #     "envs/blast.yaml"
    shell:
        """
        module load Blast/ncbi-blast/2.9.0+;
        blastp -query {input.fasta} -db {params.outgroup_db} -outfmt 6 -out {output.outgroup} -num_threads {threads} -evalue 1e-10 -max_target_seqs 5;
        """
    