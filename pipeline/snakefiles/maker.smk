
include: "blastp.smk"


MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The destination directory for the output files.
"""

MAKER_TMPDIR = "__maker_{source}_{assembly}__/"
"""
The temporary directory for the output files.
"""

localrules: 
    prep_maker,
    maker2gff3,
    vet_gff3,
    crop_ids,

rule prep_maker:
    input:
        genome = "../data/{source}/{assembly}.fasta",
        transcriptome = "../../genome_assembly/assembly/RNAseq/trinity/Trinity.fasta",
        TEs = "../edta/{source}/{assembly}.fasta.mod.EDTA.TElib.fa",

    # conda:
    #     "envs/maker.yaml"

    output: 
        ctl = temp( MAKER_TMPDIR + "maker_opts.ctl" ),
    
    params:
        script_file = "snakefiles/scripts/prep_maker.py",
        v10_CDS_proteins = "/data/courses/assembly-annotation-course/CDS_annotation/Atal_v10_CDS_proteins",
        uniprot_proteins = "/data/courses/assembly-annotation-course/CDS_annotation/uniprot-plant_reviewed.fasta",
        pTREB20 = "/data/courses/assembly-annotation-course/CDS_annotation/PTREP20",
        tmp = "\$SCRATCH",


    shell:
        """        
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;

        outdir=$(dirname {output.ctl})
        if [ ! -d $outdir ]; then mkdir -p $outdir; fi; 
        cd $outdir;
        maker -CTL;
        cd ..;

        script=$(realpath {params.script_file});
        genome=$(realpath {input.genome});
        transcriptome=$(realpath {input.transcriptome});
        TEs=$(realpath {input.TEs});
        output=$(realpath {output.ctl});


        # now we need to edit the maker_opts.ctl file
        python3 $script $output \
                 --pair genome "$genome" \
                 --pair est "$transcriptome" \
                 --pair rmlib "$TEs" \
                 --pair protein "{params.v10_CDS_proteins},{params.uniprot_proteins}" \
                 --pair repeat_protein "{params.pTREB20}" \
                 --pair est2genome 1 \
                 --pair protein2genome 1 \
                 --pair TMP "{params.tmp}" \
                 --pair cpus 10;
        """

# Currently we are running into issues with the maker environment because 
# we'd need RepBase which is now commercial and I don't want to figure out how to
# download it so we just trust that it is going to work even with the perl warnings...
rule maker:
    input: 
        ctl = rules.prep_maker.output.ctl,

    output:
        MAKER_TMPDIR + "{assembly}.maker.output/{assembly}_master_datastore_index.log",
    
    params:
        dest = MAKER_DIR 

    threads:
        100

    # conda:
    #     "envs/maker.yaml"

    shell:
        """
        
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;

        mkdir -p {params.dest};
        outdir=$(dirname {input.ctl})
        cd $outdir;
        maker -c {threads} # $(basename {input.ctl});
        cd ..;
        """



rule maker2gff3:
    input:
        log = rules.maker.output,
    output:
        MAKER_DIR + "pre_map_ids.gff3" # temp( MAKER_DIR + "pre_map_ids.gff3" )
    # conda:
    #     "envs/maker.yaml"
    shell:
        """
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;
        gff3_merge -d {input.log} -o {output};
        """

rule maker2fasta:
    input:
        log = rules.maker.output,
    output:
        proteins = temp( MAKER_DIR + "proteins.pre_map.fasta" ),
        transcripts = temp( MAKER_DIR + "transcripts.pre_map.fasta" ),

    # conda:
    #     "envs/maker.yaml"

    shell:
        """
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;

        dir=$(dirname {input.log}); cd $dir;

        fasta_merge -d $(basename {input.log}) -o {wildcards.assembly};
        cd -;
        mv $dir/{wildcards.assembly}.all.maker.proteins.fasta {output.proteins}
        mv $dir/{wildcards.assembly}.all.maker.transcripts.fasta {output.transcripts}
        """

# rule vet_gff3:
#     input:
#         gff3 = rules.maker2gff3.output,
#     output:
#         MAKER_DIR + "{assembly}.gff3"
#     script:
#         "scripts/vet_gff3.py"

rule vet_gff3:
    input:
        gff3 = rules.maker2gff3.output,
    output:
        temp( MAKER_DIR + "{assembly}.vetted.gff3" ) 
    script:
        "scripts/vet_gff3.py"

    

rule crop_ids:
    input:
        gff = rules.vet_gff3.output,
        proteins = rules.maker2fasta.output.proteins,
        transcripts = rules.maker2fasta.output.transcripts,
    output:
        transcripts = MAKER_DIR + "transcripts.fasta",
        proteins = MAKER_DIR + "proteins.fasta",
        gff = MAKER_DIR + "{assembly}.gff3",
    
    params:
        prefix = "CVI_",
    
    # conda:
    #     "envs/maker.yaml"

    shell:
        """
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;

        tmpfile=.{wildcards.assembly}.tmp.ids;
        maker_map_ids --prefix {params.prefix} {input.gff} > $tmpfile;
        map_fasta_ids $tmpfile {input.transcripts} ; mv {input.transcripts} {output.transcripts};
        map_fasta_ids $tmpfile {input.proteins} ; mv {input.proteins} {output.proteins};
        map_gff_ids $tmpfile {input.gff}; mv {input.gff} {output.gff};
        rm $tmpfile;
        """

rule map_functional:
    input: 
        ref_fasta = "/data/courses/assembly-annotation-course/CDS_annotation/uniprot-plant_reviewed.fasta",
        protein_fasta = rules.crop_ids.output.proteins,
        transcripts_fasta = rules.crop_ids.output.transcripts,
        gff = rules.crop_ids.output.gff,
        blast = MAKER_DIR + "proteins.blast",
        uniprot_db = rules.make_ref_uniprot_database.output,

    output: 
       protein_fasta = MAKER_DIR + "proteins.functional.fasta",
       transcripts_fasta = MAKER_DIR + "transcripts.functional.fasta",
        gff = MAKER_DIR + "{assembly}.functional.gff3",

    params:
        uniprot_db = "uniprot-plant_reviewed.fasta.db",
    # conda:
    #     "envs/maker.yaml"
    shell:
        """
        module load SequenceAnalysis/GenePrediction/maker/2.31.9;
        
        maker_functional_fasta {input.ref_fasta} {input.blast} {input.protein_fasta} > {output.protein_fasta};
        maker_functional_fasta {input.ref_fasta} {input.blast} {input.transcripts_fasta} > {output.transcripts_fasta};
        maker_functional_gff {input.ref_fasta} {input.blast} {input.gff} > {output.gff};
        """
