
localrules:
    copy_data,
    download_refs,
    
rule copy_data:
    input:
        "../../genome_assembly/polishing/{source}{assembly}/pilon_Illumina.fasta"
    output:
        "../data/{source}{assembly}.fasta"
    shell:
        "cp {input} {output}"

rule download_refs:
    output:
        "../reference/cds.fasta"
    params:
        url = "https://www.arabidopsis.org/download_files/Sequences/TAIR10_blastsets/TAIR10_cds_20110103_representative_gene_model",
    shell:
        """
        if [ ! -f {output} ]; then
            if [ ! -d ../reference ]; then
                mkdir ../reference
            fi
            wget -O {output} {params.url}
        fi
        """