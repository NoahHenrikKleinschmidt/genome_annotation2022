include: "tesorter.smk"

from glob import glob

localrules:
    split_copia_gypsy,
    merge_fasta_with_ref,
    annotate_tree,
    modify_identifiers,
    
rule split_copia_gypsy:
    input:
        "../tesorter/{source}/{assembly}.cls.pep"
    
    params:
        # the cls.pep files contain empty fasta headers without sequence,
        # remove these from the split files.
        remove_empty_seqs = True,

    output:
        gypsy = "../tesorter/{source}/{assembly}.cls.pep.RT.gypsy",
        copia = "../tesorter/{source}/{assembly}.cls.pep.RT.copia"
    
    script:
        "scripts/split_gypsy_copia.py"

rule merge_fasta_with_ref:
    input:
        ours = expand( "../tesorter/{{source}}/{{assembly}}.cls.pep.RT.{type}",
            type = ["gypsy", "copia"]),
        ref = expand( rules.ref_tesorter.params.prefix + ".cls.pep.RT.{type}",
            type = ["gypsy", "copia"]),
    output:
        temp( expand( "../tesorter/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged", type = ["gypsy", "copia"]) )
    run:
        for our, ref, outfile in zip( input.ours, input.ref, output ):
            shell( "cat {our} {ref} > {outfile}" )

rule modify_identifiers:
    input:
        rules.merge_fasta_with_ref.output
    output:
        temp( expand( "../tesorter/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.mod", type = ["gypsy", "copia"] ) )
    script:
        "scripts/modify_identifiers.py"

rule clustal:
    input:
        rules.modify_identifiers.output
    output:
        temp( expand( "../tesorter/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.mod.fa", type = ["gypsy", "copia"] ) )
    script:
        "scripts/clustal.py"

rule fastTree:
    input:
        rules.clustal.output
    output:
        expand( "../fasttree/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.tree", type = ["gypsy", "copia"] )
    conda:
        "envs/fasttree.yml"
    script:
        "scripts/fasttree.py"
    
rule annotate_tree:
    input:
        rules.modify_identifiers.output
    
    output:
        expand( "../fasttree/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.annotation", type = ["gypsy", "copia"] )
    
    script:
        "scripts/tree_annotation.py"

rule render_one:
    input:
        tree = "../fasttree/{source}/{assembly}.cls.pep.RT.{type}.merged.tree",
        annotation = "../fasttree/{source}/{assembly}.cls.pep.RT.{type}.merged.annotation",
    
    conda:
        "envs/phylo.yaml"
    output:
        tree = "../fasttree/{source}/{assembly}.cls.pep.RT.{type}.merged.pdf",
        legend = "../fasttree/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.legend.pdf",
    log:
        notebook = "../fasttree/{source}/{assembly}.cls.pep.RT.{type}.merged.ipynb"
    notebook:
        "notebooks/tree.py.ipynb"

rule render_all:
    input:
        expand( "../fasttree/{{source}}/{{assembly}}.cls.pep.RT.{type}.merged.pdf", type = ["gypsy", "copia"] )