
include: "maker.smk"

DUPGEN_DIR = "../dupgen/{source}/{assembly}/"
"""
The destination directory for the generated files.
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The maker output directory.
"""

DATA_DIR = "../data/{source}/"
"""
The data directory.
"""

DATASET = "cvi"
"""
The dataset name.
"""


SCRIPTS = "/data/courses/assembly-annotation-course/CDS_annotation/scripts"
"""
The directory containing provided scripts
"""

localrules:
    get_DupGen,
    prep_gff,

rule get_DupGen:
    output:
        directory( "DupGen_finder" ) 
    shell:
        """
        git clone https://github.com/qiao-xin/DupGen_finder.git
        cd DupGen_finder
        make
        chmod 775 DupGen_finder.pl
        chmod 775 DupGen_finder-unique.pl
        chmod 775 set_PATH.sh
        # source set_PATH.sh
        """

rule prep_gff:
    input:
        ingroup = rules.map_functional.output.gff,
    output:
        DUPGEN_DIR + "{assembly}.RA.gff"
    script:
        "scripts/dupgen_subset_RA.py"

rule prep_proteins_fasta:
    input:
        gff = rules.prep_gff.output,
        proteins = MAKER_DIR + "proteins.functional.fasta"
    output:
        temp( DUPGEN_DIR + "{assembly}.RA.proteins.fasta" )
    conda:
        "envs/gffread.yml"
    script:
        "scripts/dupgen_prep_proteins_fasta.py"

rule merge_gff:
    input:
        ingroup = rules.prep_gff.output,
    
    params:
        outgroup = "/data/courses/assembly-annotation-course/CDS_annotation/NNU_mRNA_single_model.gff",
        outgroup_name = "NNU",

    output:
        processed_ingroup = DUPGEN_DIR + "{assembly}.gff",
        merged = DUPGEN_DIR + "{assembly}_NNU.gff",

    script:
        "scripts/dupgen_merge_gff.py"

rule run_dupgen:
    input:  
        dupgen = rules.get_DupGen.output,
        merged_gff = rules.merge_gff.output.merged,
        gff = rules.merge_gff.output.processed_ingroup,
        blast = rules.blast_dupgen_ingroup.output.ingroup,
        blast_outgroup = rules.blast_dupgen_outgroup.output.outgroup,
    output:
        directory( DUPGEN_DIR + "out" ) 
    
    params:
        outgroup_name = rules.merge_gff.params.outgroup_name,

    threads:
        10
        
    shell:
        """
        dir=$(realpath $(dirname {input.gff}))
        outdir=$(realpath {output})
        if [ ! -d $outdir ]; then
            mkdir -p $outdir
        fi
        cd {input.dupgen}
        ./DupGen_finder.pl -i $dir -t {wildcards.assembly} -c {params.outgroup_name} -o $outdir
        """

rule get_duplicated_genes:
    input:
        genes = rules.run_dupgen.output[0] + "/{assembly}.wgd.genes",
        fasta = MAKER_DIR + "transcripts.functional.fasta"
    output:
        transcripts = DUPGEN_DIR + "duplicates/{DATASET}.wgd.genes.fa",
        proteins = DUPGEN_DIR + "duplicates/{DATASET}.wgd.genes.fa.proteins"
    shell:
        """
        cat {input.genes} | cut -f 1 > dupgen.ids

        module load UHTS/Analysis/SeqKit/0.13.2
        seqkit grep -f dupgen.ids {input.fasta} -o {output.transcripts}
        
        seqkit translate {output.transcripts} -o {output.proteins}
        sed -i 's/_frame=1/_p/' {output.proteins}

        rm dupgen.ids
        """

rule process_pairs:
    input:
        rules.run_dupgen.output[0] + "/{assembly}.wgd.pairs",
    output:
        DUPGEN_DIR + "duplicates/" + "wgd.pairs.csv"
    shell:
        """
        cut -f 1,3 {input} > {output}
        sed -i 's/RA/RA_p/g' {output}
        """

def get_split_filenames(infile):
    with open(infile) as f:
        for line in f:
            if line.startswith(">"):
                yield line.strip().split()[0][1:]

rule split_fasta:
    input:
        transcripts = rules.get_duplicated_genes.output.transcripts,
        proteins = rules.get_duplicated_genes.output.proteins,
    output:
        checkfile = DUPGEN_DIR + "duplicates/" + ".{DATASET}.split.done",
    shell:
        """
        transcripts=$(realpath {input.transcripts})
        proteins=$(realpath {input.proteins})
        cd $(dirname {input.transcripts})
        {SCRIPTS}/split_flat $transcripts
        {SCRIPTS}/split_flat $proteins
        touch $(basename {output.checkfile})
        """

rule align_bestflash:
    input:
        pairs = rules.process_pairs.output,
        checkfile = rules.split_fasta.output.checkfile,
    output:
        checkfile = DUPGEN_DIR + "duplicates/" + ".{DATASET}.bestflash.done",
    
    shell:
        """
        module add Emboss/EMBOSS/6.6.0
        pairs=$(realpath {input.pairs})
        cd $(dirname {input.pairs})
        {SCRIPTS}/bestflash_from_list $pairs
        touch $(basename {output.checkfile})
        """
rule align_paml:
    input:
        rules.align_bestflash.output.checkfile,
    output:
        checkfile = DUPGEN_DIR + "duplicates/" + ".{DATASET}.paml.done",
    params:
        prefix = "CVI",
    shell:
        """
        module add Emboss/EMBOSS/6.6.0
        cd $(dirname {input})
        {SCRIPTS}/pair_to_CDS_paml_pair {params.prefix} pair
        touch $(basename {output.checkfile})
        """

rule yn00:
    input:
        rules.align_paml.output.checkfile,
    output:
        DUPGEN_DIR + "duplicates/" + "{DATASET}.yn00.out",

    params:
        prefix = "CVI",
    shell:
        """
        module load Phylogeny/paml/4.9j
        cd $(dirname {input})
        {SCRIPTS}/PAML_yn00_from_CDS_pair {params.prefix} > $(basename {output})
        """
        
rule get_yn00_results:
    input:
        rules.yn00.output,
    output:
        DUPGEN_DIR + "duplicates/" + "{DATASET}.wgd.kaks",
    params:
        prefix = "CVI",
    shell:
        """
        awk '{{print($1,$1,$6,$7,$5)}}' {input}|sed 's/ /\t/g'|sed \
        's/__x__/\t/'|sed 's/_p//g'|cut -f 1,2,4,5,6|sed 's/dN=//'|sed 's/dS=//'|sed \
        's/omega=//'|awk '$4<5' > {output}
        """

rule combine_collinearity:
    input:
        collinearity = DUPGEN_DIR + "out/{assembly}.collinearity",
        kaks = rules.get_yn00_results.output,
    output:
        DUPGEN_DIR + "duplicates/" + "{DATASET}.collinearity.kaks",
    shell:
        """
        cp {input.collinearity} $(dirname {output})/{DATASET}.collinearity
        cd $(dirname {output})
        perl {SCRIPTS}/add_ka_ks_to_collinearity_file.pl {DATASET}
        """

rule compute_average_ks:
    input:
        rules.combine_collinearity.output,
    output:
        DUPGEN_DIR + "duplicates/" + "{DATASET}.synteny.blocks.ks.info",
    params:
        prefix = "CVI",
    shell:
        """
        cd $(dirname {input})
        perl {SCRIPTS}/compute_ks_for_synteny_blocks.pl $(basename {input})
        rm {params.prefix}* 
        """
    