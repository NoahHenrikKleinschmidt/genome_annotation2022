"""
Since bedtools getfasta is prducing weird results, I'm going to try to make my own fasta file from the gff3 file.

UPDATE
------
Alrighty, turned out the vet_gff3 rule was the probelm and bedtools was always working correctly, so this script is now obsolete and not used anymore.
"""

import subprocess
import pandas as pd

GENOME = str(snakemake.input.genome)
GFF3 = str(snakemake.input.gff3)
OUTFILE = str(snakemake.output)



def run( cmd : str ) -> str:
    """
    Run a shell command and return the stdout.
    """
    return subprocess.run(cmd, shell=True, capture_output=True).stdout.decode("utf-8")

def get_contig_range( contig : str ) -> tuple:
    """
    Get the start and end lines of a contig.
    """

    cmd = f"grep -n { contig } { GENOME } | cut -d: -f1"
    start = int( run(cmd) )
    
    cmd = f"tail -n { start+1 } { GENOME } | grep -m 1 -n '>' | cut -d: -f1"
    end = int( run(cmd) )

    return start, end

def get_contig_sequence( contig : str ) -> str:
    """
    Get the sequence of a contig.
    """
    start, end = get_contig_range( contig )
    cmd = f"tail -n { start } { GENOME } | head -n { end }"
    return run(cmd).replace("\n", "")

def read_gff3( gff3 : str ) -> pd.DataFrame:
    """
    Read a gff3 file into a pandas dataframe.
    """
    cols = ["contig", "source", "type", "start", "end", "score", "strand", "phase", "attributes"]
    df = pd.read_csv(gff3, sep="\t", names = cols)
    return df

def get_feature_range( row ) -> tuple:
    """
    Get the start and end of a feature.
    """
    start = int(row["start"]) -1
    end = int(row["end"]) -1
    if row["strand"] == "-":
        start, end = end, start
    return start, end

def get_feature_sequence( seq, row ) -> str:
    """
    Get the sequence of a feature.
    """
    start, end = get_feature_range( row )
    return seq[start:end]

def fasta_entry( seq, header ) -> str:
    """
    Make a fasta entry for a feature.
    """
    return f">{header}\n{seq}\n"

def header( row ) -> str:
    """
    Make a header for a feature.
    """
    return f"{row['contig']}:{row['start']}-{row['end']}"

if __name__ == "__main__":
    
    out = open(OUTFILE, "w")
    df = read_gff3(GFF3)


    counter = len( df.contig.unique() )

    contigs = df.groupby("contig")
    for contig, subset in contigs:
    
        print(f"Processing {contig} ({counter} remaining)")
        added_entries = set()

        seq = get_contig_sequence(contig)
       
        for i, row in subset.iterrows():
            

            if row["type"] == "contig":
                continue

            h = header(row)
            if h in added_entries:
                continue

            s = get_feature_sequence(seq, row)            
            if s == "":
                continue
            
            s = fasta_entry(s, h)
            out.write( s )

            added_entries.add( h )

        counter -= 1
    out.close()