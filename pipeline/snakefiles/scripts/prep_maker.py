"""
A script to fill in the gaps in the maker annotation pipeline.
"""

class Makerfile:
    def __init__(self, makerfile):
        self.makerfile = makerfile
        with open(makerfile) as f:
            self.content = f.read()
    
    def fill( self, key, value ):
        """
        Fill in an empty value in the makerfile.
        """
        new = [ self._fill_line(line, key, value) for line in self.content.split("\n") ]
        self.content = "\n".join(new)
   

    def write( self, outfile = None ):
        """
        Write the makerfile to a new file.
        """
        if outfile is None:
            outfile = self.makerfile
        self._final_vet()
        with open(outfile, "w") as f:
            f.write(self.content)

    @staticmethod
    def _fill_line( line, key, value ):
        """
        Fill in an empty value in a single line.
        """
        key = f"{key}="
        value = f"{key}={value} "
        
        if not line.startswith(key):
            return line

        ddx = line.find("#")
        line = f"{value} {line[ddx:]}" 
        return line

    def _final_vet(self):
        """
        Check that the makerfile is ready for submission.
        """
        self.content = self.content.replace("= =","=")
        self.content = self.content.replace("==","=")

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("makerfile", help="The makerfile to fill in.")
    parser.add_argument("--pair", help="A pair of key and value to fill in.", nargs=2, action="append")
    args = parser.parse_args()
    
    makerfile = Makerfile(args.makerfile)
    for key, value in args.pair:
        print( f"Filling in {key} with {value}" )
        makerfile.fill(key, value)
    makerfile.write()