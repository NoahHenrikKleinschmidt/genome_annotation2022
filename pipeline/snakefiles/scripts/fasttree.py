"""
This script makes use of the fastree conda environment which uses 2.1.11 instead of the cluster's 2.1.10
"""

import os

if not os.path.isdir( f"../fasttree/{snakemake.wildcards.source}" ):
    os.makedirs( f"../fasttree/{snakemake.wildcards.source}" )

for infile, outfile in zip( snakemake.input, snakemake.output ):
    os.system( f"FastTree < {infile} > {outfile}" )
