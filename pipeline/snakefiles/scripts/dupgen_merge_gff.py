"""
This script processess and merges an ingroup and outgroup GFF file for dupgen
"""
import pandas as pd
import os

input = snakemake.input
output = snakemake.output
params = snakemake.params
wildcards = snakemake.wildcards

# os.system( f"mkdir -p {os.path.dirname(str(output.merged))}" )

ingroup = pd.read_csv( str(input.ingroup), sep = "\t", header = None, dtype=str)
outgroup = pd.read_csv( str(params.outgroup), sep = "\t", header = None, dtype=str )

df = ingroup
df = df[ [0, 8, 3, 4] ]
df.rename( columns = {0: "chr", 8 : "id", 3 : "start", 4 : "end"}, inplace = True )
outgroup.columns = df.columns

df["id"] = df["id"].str.split("ID=", expand = True)[1]
df["id"] = df["id"].str.split(";", expand = True)[0]

df["chr"] = wildcards.assembly + "-" + df["chr"].astype(str)
outgroup["chr"] = params.outgroup_name + "-" + outgroup["chr"].astype(str)

df.to_csv( str(output.processed_ingroup), sep = "\t", header = False, index = False )

df = pd.concat( [df, outgroup], axis = 0 )
df.to_csv( str(output.merged), sep = "\t", header = False, index = False )
