"""
This script generates a fasta file of the protein sequences for the ingroup
"""

import pandas as pd

gff = pd.read_csv( str(snakemake.input.gff), sep = "\t", header = None, dtype=str)

entries = gff[8].str.split(";", expand = True)[0].str.split("=", expand = True)[1]

infile = open( str(snakemake.input.proteins), "r" )
outfile = open( str(snakemake.output), "w" )

seqs = infile.read().split(">")[1:]

for seq in seqs:
    start = seq[ :seq.find(" ") ]
    if start in entries.values:
        outfile.write( ">" + seq )

infile.close()
outfile.close()
