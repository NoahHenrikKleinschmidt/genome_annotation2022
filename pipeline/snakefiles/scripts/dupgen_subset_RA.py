"""
This script processess the ingroup GFF file for dupgen
"""
import pandas as pd
import os

input = snakemake.input
output = snakemake.output
params = snakemake.params
wildcards = snakemake.wildcards

# os.system( f"mkdir -p {os.path.dirname(str(output.merged))}" )

ingroup = pd.read_csv( str(input.ingroup), sep = "\t", header = None, dtype=str)
df = ingroup

df = df[ df[2] == "mRNA" ]
df = df[ df[8].str.contains("ID=") ]
df = df[ df[8].str.contains("-RA") ]

df.to_csv( str(output), sep = "\t", header = False, index = False )