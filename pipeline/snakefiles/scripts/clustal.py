"""
This script calls clustal omega to align a set of sequences.
"""

import os
input = snakemake.input
output = snakemake.output

for infile, outfile in zip( input, output ):
    os.system(  f"""
                module load SequenceAnalysis/MultipleSequenceAlignment/clustal-omega/1.2.4;
                clustalo -i {infile} -o {outfile} ;
                """  )