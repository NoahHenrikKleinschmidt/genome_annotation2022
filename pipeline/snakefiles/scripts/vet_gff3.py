"""
This script will ensure that the maker gff files are all properly formated
"""
input = snakemake.input
output = snakemake.output

infile = open( str(input.gff3), "r" )
outfile = open( str(output), "w" )

for line in infile:
    
    if "##FASTA" in line:
        break

    elif line.startswith("#"):
        continue

    else:
        if len( line.split("\t") ) == 9:

            outfile.write( line )

        else:
            print( "ERROR: line does not have 9 columns, skipping...\n"+line )
            continue

infile.close()
outfile.close()
