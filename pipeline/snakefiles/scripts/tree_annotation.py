"""
This script makes an annotation file for the FastTree output.
"""

read = lambda x: ( i for i in open( str(x), "r" ).readlines() if i.startswith(">") )
get_node = lambda line: line.split(" ")[0].replace(">", "").replace("(+)", ".p.").replace("(-)",".m.").strip()
get_clade = lambda node: node.split("|")[-1].strip()

for input, output in zip( snakemake.input, snakemake.output ):
    
    headers = read(input)
    with open(output, "w") as f:
        f.write( "node\tclade\n" )
        for header in headers:
            node = get_node(header)
            clade = get_clade(node)
            f.write( f"{node}\t{clade}\n" ) 

    