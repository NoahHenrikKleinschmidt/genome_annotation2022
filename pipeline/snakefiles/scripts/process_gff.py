"""
The script for rule process_gff.

It shuffles columns 3 and 9 and reduces the annotation to only the Name attribute.

Also, an odd quirk emerged, in that the scaffold names in the GFF are cropped from tig00001_pilon to just tig00001_pil. 
But that only happend for the canu gff files, not the flye ones... This script fixes that.
"""

import pandas as pd

df = pd.read_csv( str(snakemake.input), sep="\t", header=None, comment = "#" )
cols = list( df.columns )

# Fix scaffold names
if df.iloc[0,0].endswith("_pil"):
    df[0] = df[0].str.replace("_pil", "_pilon")

# reduce the annotation to only the Name attribute
df[ cols[8] ] = df[ cols[8] ].apply( lambda x: x.split("Name=")[1].split(";")[0] )

# shuffle columns 3 and 9
cols[2], cols[8] = cols[8], cols[2]
df = df[cols]


df.to_csv( str(snakemake.output), sep="\t", header=False, index=False )