"""
This script splits the gypsy and copia 
families into separate files, starting from one .cls.pep file from TEsorter.¨

It can also remove any empty sequences from the files.
"""

# ----------------------------------------------------------------
#                           Main functions
# ----------------------------------------------------------------

def parse_file( filename, remove_empty ):
    if remove_empty:
        return _parse_without_empty( filename )
    else:
        return _parse_with_empty( filename )

def get_copia( entries ):
    entries = _get_family( entries, "Copia" )
    entries = _get_family( entries, "Ty1-RT" )
    return entries

def get_gypsy( entries ):
    entries = _get_family( entries, "Gypsy" )
    entries = _get_family( entries, "Ty3-RT" )
    return entries
    
def write_file( filename, entries ):
    with open( str(filename), "w" ) as f:
        for i in entries:
            f.write( f"{i[0]}\n{i[1]}\n" ) 

# ----------------------------------------------------------------
#                           Auxiliary functions
# ----------------------------------------------------------------

def _parse_without_empty( filename ):
    for i in _parse_with_empty( filename ):
        if i[1] != "":
            yield i

def _parse_with_empty( filename ):

    with open( str(filename), "r" ) as f:
        lines = f.readlines()

    i = 0
    while i < len(lines):
        if lines[i].startswith(">"):
            header = lines[i].strip()
            seq = ""
            while i < len(lines)-1 and not lines[i+1].startswith(">"):
                seq += lines[i+1].strip()
                i += 1
            yield header, seq
        i += 1

def _get_family( entries, family ):
    for i in entries:
        if family in i[0]:
            yield i


# ----------------------------------------------------------------
#                           Main
# ----------------------------------------------------------------

if __name__ == "__main__":

    # since we are using generators we need to read the file twice
    # since generators get used up...

    copia = parse_file( snakemake.input, snakemake.params.remove_empty_seqs )
    gypsy = parse_file( snakemake.input, snakemake.params.remove_empty_seqs )

    copia = get_copia( copia )
    gypsy = get_gypsy( gypsy )

    write_file( snakemake.output.gypsy, gypsy )
    write_file( snakemake.output.copia, copia )