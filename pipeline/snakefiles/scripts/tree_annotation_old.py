"""
This script makes an annotation file for the FastTree output.
"""

import seaborn as sns

TEMPLATEFILE = "../fasttree/dataset_color_strip_template.txt"

template = lambda : open( TEMPLATEFILE, "r" ).read()
read = lambda x: [ i.split(" ")[0] for i in open( str(x), "r" ).read().split(">")[1:] ] 
get_clade = lambda x: x.split("|")[-1]
get_clades = lambda x: [ get_clade(i) for i in x ]
add_entry = lambda node, color, annotation: f"{ annotation }\n{ node } { color }"

for input, output in zip( snakemake.input, snakemake.output ):
    
    annotation = template()
    headers = read(input)
    clades = get_clades(headers)

    colors = sns.color_palette("GnBu", len(set(clades)) ).as_hex()
    color_dict = dict(zip(set(clades), colors))

    for header in headers:
        color = color_dict[ get_clade(header) ]
        annotation = add_entry(header, color, annotation)
    
    with open(output, "w") as f:
        f.write(annotation)