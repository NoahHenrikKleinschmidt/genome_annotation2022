"""
This script fixes fasta headers and makes sure that all identifers are unique (for some reason 
canu fasta files have duplicated identifiers which makes them unusable for FastTree...)
"""


read = lambda x: open(x, "r").readlines()
# header = lambda x: x.split(" ")[0][1:]
header = lambda x: x.split("(")[0]

def get_headers( lines ):

    raws = list( header(i) for i in lines if i.startswith(">") )

    headers = {}
    for i in raws:
        if i in headers:
            headers[i] += 1
        else:
            headers[i] = 1
    
    return headers

def make_unique(headers, line):
    if line.startswith(">"):
        _header = header(line)
        _header_count = headers.get( _header, 0 )
        if _header_count > 1:
            line = line.replace( _header, _header + str(_header_count-1) )
            headers[_header] -= 1
        
        line = line.split(" ")[0] + "\n"

        line = line.replace("(+)", ".p.")
        line = line.replace("(-)", ".m.")
    return line

if __name__ == "__main__":


    for infile, outfile in zip( list(snakemake.input), list(snakemake.output) ):        

        lines = read( infile )
        outfile = open( outfile, "w" )
        
        headers = get_headers( lines )
        for line in lines:
            
            line = line.replace(":","_")
            line = make_unique(headers, line)
            outfile.write( line )
        
        outfile.close()