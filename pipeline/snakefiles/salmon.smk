
SALMON_DIR = "../salmon/{source}/{assembly}/"
"""
The directory to save results in
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The maker directory
"""

rule index:
    input: 
        ancient( MAKER_DIR + "transcripts.fasta" ),
    output:
        directory( SALMON_DIR + "salmon.idx"),
    conda: 
        "envs/salmon.yml",
    params:
        k = 31,
    shell: 
        """
        if [ ! -d "../salmon" ]; then mkdir -p ../salmon; fi
        salmon index -t {input} -i {output} -k {params.k}
        """

rule salmon:
    input:
        raw_fwd = "../../genome_assembly/data/RNAseq__R1.fastq.gz",
        raw_rev = "../../genome_assembly/data/RNAseq__R2.fastq.gz",
        index = rules.index.output,
    output:
        directory(SALMON_DIR + "salmon.quant/quant.sf")   
    params:
        threads = 35,
    conda:
        "envs/salmon.yml",
    shell:
        "salmon quant -i {input.index} -l A -1 {input.raw_fwd} -2 {input.raw_rev} -p {params.threads} --validateMappings -o {output}"
