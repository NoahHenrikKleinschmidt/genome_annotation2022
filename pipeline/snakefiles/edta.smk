import os

source = "pacbio"
assemblers = ("canu", "flye")

base = "/data/users/noahkleinschmidt/genome_annotation"

containers = "/data/courses/assembly-annotation-course/containers2"
EDTA = f"{containers}/EDTA_v1.9.6.sif"

rule run_EDTA:
    input:
        fasta = "../data/{source}/{assembly}.fasta",
        ref = "../reference/cds.fasta"
    output:
        "../edta/{source}/{assembly}.edta",
        "../edta/{source}/{assembly}.fasta.mod.EDTA.TElib.fa",
    params:
        EDTA = EDTA,
        containers = containers,
        base = base
    
    threads: 10
    shell:
        """
        if [ ! -d ../edta/{wildcards.source} ]; then
            mkdir -p ../edta/{wildcards.source}
        fi;

        input=$(realpath {input.fasta});
        ref=$(realpath {input.ref});
        
        singularity exec \
        --bind {params.containers} \
        --bind {params.base} \
        {params.EDTA} EDTA.pl \
        --genome  $input \
        --species others \
        --step all \
        --cds $ref \
        --anno 1 \
        --threads  {threads} ;

        # the simple file moving did not work properly,
        # so we use this slightly overkill verison that does the trick...
        edta_files=$( ls );
        for file in $edta_files; do
            if [[ $file == {wildcards.assembly}* ]]; then
                mv $file ../edta/{wildcards.source}/$file;
            fi;
        done;
        touch {output};
        """