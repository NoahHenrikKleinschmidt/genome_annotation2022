
SUMMARY_DIR = "../summary/{source}/{assembly}/"
"""
The destination directory for the summary files.
"""

SALMON_DIR = "../salmon/{source}/{assembly}/"
"""
The directory containing the Salmon output files.
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The directory containing the Maker output files.
"""

STAR_DIR = "../star/{source}/{assembly}/"
"""
The directory containing the STAR output files.
"""

BUSCO_DIR = "../busco/{source}/{assembly}/"
"""
The directory containing the BUSCO output files.
"""

import glob

localrules:
    salmon_volcano,
    star_stats,
    busco_stats
    
rule salmon_volcano:
    input:
        directory(SALMON_DIR + "salmon.quant/quant.sf")
    output:
        counts = report( SUMMARY_DIR + "salmon.expressed.tsv", caption = "Salmon expressed genes" ),
        html = report( SUMMARY_DIR + "salmon.html", caption = "Salmon volcano plot" ),
        png = SUMMARY_DIR + "salmon.png"
    params:
        expr_threshold = 1,
    log:
        notebook = SUMMARY_DIR + "salmon.ipynb",
    notebook:
        "notebooks/salmon_volcano.py.ipynb"

rule star_stats:
    input:
        STAR_DIR + "{assembly}Log.final.out"
    output:
        counts = report( SUMMARY_DIR + "star.stats.tsv", caption = "STAR base-statistics" ),
        html = report( SUMMARY_DIR + "star.html", caption = "STAR base-statistics" ),
        png = SUMMARY_DIR + "star.png"
    
    log:
        notebook = SUMMARY_DIR + "star.ipynb",
    notebook:
        "notebooks/star_stats.py.ipynb"

rule busco_stats:
    input:
        indir = BUSCO_DIR,
        summary = lambda wildcards: glob.glob( BUSCO_DIR.format( source = wildcards.source, assembly = wildcards.assembly) + "short_summary*.json" )
    output:
        counts = report( SUMMARY_DIR + "busco.stats.tsv", caption = "BUSCO base-statistics" ),
        html = report( SUMMARY_DIR + "busco.html", caption = "BUSCO base-statistics" ),
        png = SUMMARY_DIR + "busco.png"
    
    log:
        notebook = SUMMARY_DIR + "busco.ipynb",
    notebook:
        "notebooks/busco_stats.py.ipynb"
   