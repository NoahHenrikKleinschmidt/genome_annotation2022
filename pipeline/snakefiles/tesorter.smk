
localrules:
    get_transposons,
    process_gff,

rule get_transposons:
    input: "../edta/{source}/{assembly}.fasta.mod.EDTA.TEanno.gff3"
    output: gff = temp( "../edta/{source}/{assembly}.mod.EDTA.TEanno.transposons.gff3.tmp" )
    shell:
        """
        # extract transposon features
        awk '$3~/retrotransposon/' {input} > {output.gff};
        grep -v LTR {input} >> {output.gff};
        """

rule process_gff:
    input: 
        rules.get_transposons.output.gff
    output: 
        "../edta/{source}/{assembly}.mod.EDTA.TEanno.transposons.gff3"
    script:
        "scripts/process_gff.py"
        

rule get_fasta:
    input:
        fasta = "../data/{source}/{assembly}.fasta",
        gff = rules.process_gff.output,
    output:
        "../fasta/{source}/{assembly}.mod.EDTA.TEanno.transposons.fa"
    shell:
        """
        if [ ! -d $(dirname output) ]; then mkdir -p $(dirname output); fi;

        module load UHTS/Analysis/BEDTools/2.29.2;
        bedtools getfasta -fi {input.fasta} -bed {input.gff} -fo {output} -s -nameOnly
        """

rule tesorter:
    input:
        rules.get_fasta.output,
    conda:
        "envs/tesorter.yml"
    output:
        temp( "../tesorter/{source}/.{assembly}.checkfile" ) # this is a dummy file
    params:
        database = "rexdb-plant",
        prefix = "../tesorter/{source}/{assembly}", 
    threads:
        8
    shell:
        """
        TEsorter {input} -db {params.database} -pre {params.prefix} -p {threads};
        touch {output}
        """

rule ref_tesorter:
    input:
        "/data/courses/assembly-annotation-course/CDS_annotation/Brassicaceae_repbase_all_march2019.fasta"
    conda:
        "envs/tesorter.yml"
    output:
        temp( "../tesorter/ref/.checkfile" ) # this is a dummy file
    params:
        database = rules.tesorter.params.database,
        prefix = "../tesorter/ref/Brassicaceae_repbase_all_march2019",
    threads:
        8
    shell:
        """
        TEsorter {input} -db {params.database} -pre {params.prefix} -p {threads};
        touch {output}
        """

