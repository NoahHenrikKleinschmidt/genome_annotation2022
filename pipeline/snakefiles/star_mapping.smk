
include: "maker.smk"

STAR_DIR = "../star/{source}/{assembly}/"
"""
The destination directory for the generated files.
"""

MAKER_DIR = "../maker/{source}/{assembly}/"
"""
The maker output directory.
"""

localrules:
    gff3_to_gtf,

rule gff3_to_gtf:
    input:
        rules.map_functional.output
    output:
        str(rules.vet_gff3.output).replace(".gff3", ".gtf")
    conda:
        "envs/gffread.yml"
    resources:
        tmpdir = "$SCRATCH"
    shell:
        "gffread {input} -T -o {output}"


rule generate_genome:
    input:
        fasta = "../data/{source}/{assembly}.fasta",
        gtf = rules.gff3_to_gtf.output
    output:
        directory( STAR_DIR + "genome" ) 
    threads:
        8
    resources:
        tmpdir = "$SCRATCH"
    params:
        overhang = 100,
        genomeSAindexNbases = 12 # was recommended by the logs
    shell:
        """
        if [ ! -d {output} ]; then
            mkdir -p {output}
        fi

        outdir=genome
        genome=$(realpath {input.fasta})
        gtf=$(realpath {input.gtf})
        cd $(dirname {output});
        
        module add UHTS/Aligner/STAR/2.7.9a;

        STAR --runMode genomeGenerate --genomeDir $outdir --genomeFastaFiles $genome --sjdbGTFfile $gtf --sjdbOverhang {params.overhang}  --genomeSAindexNbases {params.genomeSAindexNbases} --runThreadN {threads};
        """

rule map_star:
    input:
        fastq = expand( "../../genome_assembly/data/RNAseq__R{dir}.fastq.gz", dir = (1,2) ),
        genome = rules.generate_genome.output,
    output:
        bam = STAR_DIR + "{assembly}Aligned.sortedByCoord.out.bam",
        log = STAR_DIR + "{assembly}Log.final.out",
    
    threads:
        10
        
    resources:
        tmpdir = "$SCRATCH",

    params:
        multimap_max = 10,
        intron_maxlength = 60000,
        mismatch_max_ratio = 0.01,

    shell:
        """
        module add UHTS/Aligner/STAR/2.7.9a;

        genome=$(realpath {input.genome});
        fastq=$(realpath {input.fastq});
        cd {input.genome}; cd ..;

        STAR --genomeDir $genome --readFilesIn $fastq --outFileNamePrefix {wildcards.assembly} --outSAMtype BAM SortedByCoordinate --outFilterIntronMotifs RemoveNoncanonicalUnannotated --outFilterMultimapNmax {params.multimap_max} --alignIntronMax {params.intron_maxlength} --runThreadN {threads} --readFilesCommand zcat;
        """
    