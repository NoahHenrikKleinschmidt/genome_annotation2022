"""
This script keeps track of the progress that maker has made in the annotation
"""

import os, time
import glob
from qpcr._auxiliary.graphical import make_layout_from_list
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from datetime import datetime
import argparse

import warnings
warnings.filterwarnings('ignore')

REF_SIZES = {
    "pacbio_canu" : 822,
    "pacbio_flye" : 428,
}
"""
The number of master log entries of the previously finished run...
"""

def cli():
    """
    Command line interface
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--wait", type=int, default=60, help="Wait for x seconds before running the script again. Default: 60")
    parser.add_argument("--clear", action="store_true", help="Clear the maker progress files")
    args = parser.parse_args()
    return args

parent = os.path.dirname( os.path.realpath(__file__) ) 
parent = os.path.dirname(parent)
os.chdir(parent)
print( os.getcwd() )

sns.set_palette( "colorblind" )

def get_maker_logs():
    """
    Get the maker directories
    """
    dirs = glob.glob("__maker*/*output", recursive = True )
    logs = [ glob.glob(f"{d}/*master_datastore_index.log", recursive = True ) for d in dirs ]
    logs = [ l[0] for l in logs if len(l) > 0 ]
    return logs

def get_maker_name( log ):
    """
    Get the maker run name
    """
    return log.split("/")[0].replace("__maker_", "").replace("__", "") 

def get_maker_progress( logs ):
    """
    Get the maker progress
    """
    total = []
    for log in logs:
        df = pd.read_csv(log, sep="\t", names = ["c1", "c2", "status"])
        
        finished = df[ df["status"] == "FINISHED" ]
        started = df[ df["status"] == "STARTED" ][ ~ df["c1"].isin( finished["c1"] ) ]
        failed = df[ df["status"] == "FAILED" ][ ~ df["c1"].isin( finished["c1"] ) ]
    
        total.append( [ get_maker_name(log), len(started), len(finished), len(failed) ] )

    total = pd.DataFrame(total, columns = ["name", "started", "finished", "failed"])
    return total

def plot_maker_progress_pie( total ):
    """
    Plot the maker progress
    """
    cols, rows = make_layout_from_list( total["name"].tolist() )
    fig, axs = plt.subplots( cols, rows, figsize = (rows*8, cols*8) )

    for name, (_, df), ax in zip( total["name"].tolist(), total.iterrows(), axs.flatten() ):
        n = df['started'] + df['finished'] + df['failed']
        if name in REF_SIZES:
            df["finished"] = df["finished"] / REF_SIZES[name]
            df["failed"] = df["failed"] / REF_SIZES[name]
    
        ax.pie( df[ ["started", "finished", "failed"] ], labels = ["started", "finished", "failed"] )
        ax.set_title( name + f" (n = {n})" )

    plt.tight_layout()
    plt.savefig("../maker/" + "maker_progress.png")

def store_maker_progress( total ):
    """
    Store the maker progress
    """
    total["timestamp"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if os.path.exists("../maker/" + "maker_progress.csv"):
        current = pd.read_csv("../maker/" + "maker_progress.csv", index_col = False)
        total = pd.concat( [current, total] )
    total.to_csv("../maker/" + "maker_progress.csv", index = False)

def plot_maker_progress_timeframe():
    """
    Plot the maker progress over time
    """
    total = pd.read_csv("../maker/" + "maker_progress.csv", index_col = False)
    total["timestamp"] = pd.to_datetime(total["timestamp"])
    total = total.sort_values("timestamp")

    sns.set_style( "whitegrid" )
    cols, rows = make_layout_from_list( total["name"].unique() )
    fig, axs = plt.subplots( cols, rows, figsize = (rows*8, cols*8) )

    for (name, df), ax in zip( total.groupby("name"), axs.flatten() ):
        # ax.plot( df["timestamp"], df["started"], label = "started" )
        ax.plot( df["timestamp"], df["finished"], label = "finished" )
        ax.plot( df["timestamp"], df["failed"], label = "failed" )
        ax.set_title( name )
        ax.legend() 
    plt.tight_layout()
    plt.savefig("../maker/" + "maker_progress_timeframe.png")

if __name__ == "__main__":

    args = cli()

    if args.clear:
        os.system("rm ../maker/maker_progress*")
        exit()

    print( "Tracking maker progress, use ctrl-c to exit" )
    while True:
            
        logs = get_maker_logs()
        df = get_maker_progress( logs )
        plot_maker_progress_pie( df )
        store_maker_progress( df )
        plot_maker_progress_timeframe()

        time.sleep( args.wait )