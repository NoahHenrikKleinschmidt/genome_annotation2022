# Genome Annotation Course

This is the project directory for part II - genome annotation. Of the Genome Assembly and Annotation Course. 

The repository for the part I is available at [gitlab as well](https://gitlab.com/NoahHenrikKleinschmidt/genome_assembly2022). 


## Data

This project uses the polished data from the previous part. The pipeline includes steps to get the data from the part I project directory on the cluster. This repository contains a compressed version of the data as a `tar.gz` that can be unpacked instead. 

## Pipeline

The main _Snakefile_ is located inside the `pipeline` directory and the entire pipeline is designed to be run from there.